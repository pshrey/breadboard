finishedStep = stepFactory.createStep()

finishedStep.run = {
	println "finishedStep.run"
  
    // Stop dropping players
    a.setDropPlayers(false)
  
    a.addEvent("GameOver", ["curRound":curRound])
    g.E.each { e->
      e.private(v, [current:"-1", arrow: ""])
    }
    
    g.V.filter{it.active}.each { player->
      a.addEvent("FinalScore", [pid:player.id, score:player.private.score])
      player.text = c.get("PostStudySurvey")
      
      a.add(player, createSurveyQuestion(q7, player))
      a.add(player, createSurveyQuestion(q8, player))
      a.add(player, createSurveyQuestion(q9, player))
      a.add(player, createSurveyQuestion(q10, player))
      a.add(player, createSurveyQuestion(q11, player))
      a.add(player, createSurveyQuestion(q12, player))
      a.add(player, createSurveyQuestion(q13, player))
      a.add(player, createSurveyQuestion(q14, player))
      a.add(player, createSurveyQuestion(q15, player))
      a.add(player, createSurveyQuestion(q16, player))
      a.add(player, createSurveyQuestion(q17, player))
      a.add(player, createSurveyQuestion(q18, player))
      a.add(player, createSurveyQuestion(q19, player))
      
      /*a.add(player, [name: "Next", result: {
        def dollars = (player.private.score / 1000)
        player.text = c.get("Finished", currency.format(dollars), g.getSubmitForm(player, dollars, "completed", amtSandbox, true))
      }])*/
      
    }
}

finishedStep.done = {
	println "finishedStep.done"
    println("Done")
}
