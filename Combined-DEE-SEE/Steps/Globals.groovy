import groovy.json.JsonSlurper

use (TimerMethods) {
  new Timer().runEvery(10000, 10000) {
    g.V.each { it.timestamp = (new java.util.Date().toString())  }
  }
}


experimentData = new File("Data/experimentData" + (new java.util.Date().toString()) + ".txt")
experimentData.createNewFile()

public static void log(String s, experimentData){
	experimentData.withWriterAppend { writer->
		writer.writeLine (s)
	} 
}

amtSandbox = false
curStep = "join"

def setEdgeDefaults(e) {
  e.tobreak = false
  e.tomake = false
  e.coopcount = 0
  e.toconfirm = false
  e.breakTarget = []
  e.bothV.each { v->
    e.private(v, [current:"0"])
  }
}

def getCoopRate(player, neighbor) {
  def showInformation = false
  
  if (treatment == 3) {
    showInformation = true 
  } else if (treatment == 2) {
    player.neighbors.each { n1->
      n1.neighbors.each { n2->
        if (n2 == neighbor) {
          showInformation = true
        }
      }
    }
  }
  
  def returnVal = [score: neighbor.score + ": 50%", color: "a46-55"]
  if (showInformation) {
    def coopCount = (showPreviousRound) ? neighbor.private.coopCount : neighbor.private.totalCoopCount
    def defectCount = (showPreviousRound) ? neighbor.private.defectCount : neighbor.private.totalDefectCount
    //println("coopCount: " + coopCount + " defectCount: " + defectCount)
    if (coopCount == 0 && defectCount == 0) return returnVal
    def numericScore = Math.round((coopCount / (coopCount + defectCount)) * 100)
    returnVal.score = neighbor.score + ": " + numericScore.toString() + "%" 
    switch (numericScore) {
      case {it > 95}:
        returnVal.color = "a96-100"
        break
      case {it > 85}:
        returnVal.color = "a86-95"
        break
      case {it > 75}:
        returnVal.color = "a76-85"
        break
      case {it > 65}:
        returnVal.color = "a66-75"
        break
      case {it > 55}:
        returnVal.color = "a56-65"
        break
      case {it > 45}:
        returnVal.color = "a46-55"
        break
      case {it > 35}:
        returnVal.color = "b36-45"
        break
      case {it > 25}:
        returnVal.color = "b26-35"
        break
      case {it > 15}:
        returnVal.color = "b16-25"
        break
      case {it > 5}:
        returnVal.color = "b6-15"
        break
      default:
        returnVal.color = "b0-5"
        break
    }
  } else {
    returnVal = [score: neighbor.score, color: "a46-55"]
  }
  
  return returnVal
}
