resultsStep = stepFactory.createStep()

resultsStep.run = {
  println "resultsStep.run: ${curRound}"
  a.addEvent("StepStart", ["step":"resultsStep","curRound":curRound])
  curStep = "results"
  
  
  g.V.filter{it.active}.each { v->
    
    v.private.score += v.private.earned
    label = v.score.substring(0,1)
    v.score = label + ": " + v.private.score
    
    
    v.neighbors.each { n->
      def curEdge = g.getEdge(n, v)
      curEdge.private(v, [current:"0"])
      def nCoop =  v.private["coop_" + n.id]
      
      
      if (nCoop == -1) {
        n.private.nDefectCount++
        curEdge.private(v, ["arrow":n.id + ",red"])
      }
      
      if (nCoop == 1) {
        n.private.nCoopCount++
          curEdge.private(n, ["animate":curRound + "," + (v.private.neighborReceive) + "," + v.getId() + "," + n.getId()])
        curEdge.private(v, ["arrow":n.id + ",green"])
      }
      
      v["coop_" + n.id] = nCoop
      }
  }
  
  g.V.filter{it.active}.each { v->
    if(individualChoice)	{ //HJ
      v.text = c.get("Results")
      v.text += v.private.resultMessage
    } else { //not individual choice
    	v.text = c.get("ResultsStep",                 
               (v.private.payment),
               (v.private.neighborReceive)
               )
    } //end of HJ fix
    
    //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
    v.neighbors.each { n->
        v.text += c.get("ResultsMulti", n.score.substring(0,1),n.private["amountPaid_" + v.id],n.private["paymentSent_" + v.id],n.private["totalTo_" + v.id])
    }
    v.text += c.get("Next")
    a.add(v, [name: "Next", result: {
      v.text += "<p>Please wait for the other players to click 'Next'.</p>"
    }])
  }
}

resultsStep.done = {
  println "resultsStep.done"
  g.V.filter{it.active}.each { v->

    // Remove arrows
    v.neighbors.each { n->
      def curEdge = g.getEdge(v,n)
      curEdge.private(v, ["arrow":""])
    }
  }
	cutTiesStep.start()
}