a.setDropPlayers(false)
a.setIdleTime(30) 
a.setDropPlayerClosure({ player ->
  a.addEvent("DropPlayer", ["pid": (player.id)])
  player.text = c.get("Dropped")
  // drop the player from the graph
  // player.active = false
  player.dropped = true
  // a.remove(player.id)
  // player.getEdges(Direction.BOTH).each { g.removeEdge(it) }
  // or, take over the player with random AI
  a.ai.add(player, { bot->
    if (bot.getProperty("choices")) {
      def choices = bot.getProperty("choices")
      // random choice
      def choice = choices[r.nextInt(choices.size())]
      if (curStep == "cooperation") {
        // choose based on past cooperation behavior
        def totalChoices = bot.private.totalCoopCount + bot.private.totalDefectCount
        def percentCooperate = (totalChoices==0)?0.5:(bot.private.totalCoopCount / totalChoices)
        // Prevent previous in-round cooperation choices from changing this value
        if (bot.getProperty("percentCooperate") == null) {
          bot.percentCooperate = percentCooperate
        } else {
          percentCooperate = bot.percentCooperate
        }
        //println("percentCooperate: " + percentCooperate)
        if (r.nextDouble() < percentCooperate) {
          choice = choices[0] 
        } else {
          choice = choices[1]
        }
      } 
      a.choose(choice.uid, null)
    }
  })
})
