graphStep = stepFactory.createStep()

graphStep.run = {
  println "graphStep.run"
  a.addEvent("StepStart", ["step":"graphStep","curRound":curRound])
  curStep = "graphStep"

  
  //commenting for AI testing
  def nActivePlayers = g.V.filter{it.active}.count()

  if (nActivePlayers > maxPlayers) {
    // Drop players in excess of maxPlayers
    def nToDrop = (int)(nActivePlayers - maxPlayers)
    g.V.filter{it.active}.shuffle.next(nToDrop).each { v->
      a.setDropPlayers(true)
      v.active = false
      a.remove(v)
      v.text = c.get("TooManyPlayers", g.getSubmitForm(v, 0, "TooManyPlayers", amtSandbox, false))
    }
  }
  
  if (nActivePlayers < minPlayers) {
    g.V.filter{it.active}.each { v->
      a.setDropPlayers(true)
      v.active = false
      a.remove(v)
      v.text = c.get("NotEnoughPlayers", g.getSubmitForm(v, 0, "NotEnoughPlayers", amtSandbox, false))
    }
    beginGame = false
  }
 
  
  if (beginGame) {
    //1: small world, 2: pref. attachment, 3: random
    if (graphType == 1) {
      g.wattsStrogatz(k, p)
    }

    if (graphType == 2) {
      g.barbasiAlbert(v)
    }

    if (graphType == 3) {
     g.random(connectivity) 
    }

    g.E.each { e->
      setEdgeDefaults(e)
    }

	println "47"
    def i = 0
    def label = 'A'
    def field = []
    def players = g.V.filter{it.active}.count() as int
      
    if(endowment){

      new File("Data/out2.csv").splitEachLine(",") {fields ->
          field[i] = fields
          i++
      }
    }
    i = 1
    
    g.V.filter{it.active}.shuffle.each { v->
      if(endowment){
       	 v.private.score = field[i][players-9] as int //outcsv2 starts assumes atleast 10 players
      } else {
       	 v.private.score = initScore
      }
      v.score = label + ": " + v.private.score
      v.label = label
      g.V.filter{it.active}.each { n->
      	v["coop_" + n.id] = 0    
        v.private["coop_" + n.id] = 0
        v.private["totalTo_" + n.id] = 0
      
      }
      label++  
      i++
    }
    
    
   
  }
  
  
  
  
}

graphStep.done = {
  //println "graphStep.done"
  if (beginGame) {
    cooperationStep.start()
  }
}
