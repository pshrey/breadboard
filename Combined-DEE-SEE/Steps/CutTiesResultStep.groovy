cutTiesResultStep = stepFactory.createStep()

cutTiesResultStep.run = {
  println("cutTiesResultsStep.run: ${curRound}")
  a.addEvent("StepStart", ["step":"cutTiesResultsStep","curRound":curRound])
  curStep = "cutTiesResults"
	
 
  if(dynamic){
   //new 
   if (curRound %3 == 0) {
    g.V.filter{it.active}.each { v->
      v.text = c.get("CutTiesResultsStep", v.private.cutCount, v.private.nCutCount)
      a.add(v, [name: "Next", result: {
        v.text += "<p>Please wait for the other players to click 'Next'.</p>"
      }])
    }
   }
  }
  
  g.E.each { e->
    if (e.tobreak) {
      e.breaking = 1
      e.bothV.each { v->
        if (e.breakTarget.size() > 1) {
          e.private(v, [arrow:"both"])
        } else {
      	  e.private(v, [arrow:e.breakTarget[0].id])
        }
      }
    }
  }
} //new curRound

cutTiesResultStep.done = {
  println("cutTiesResultsStep.done")
  g.E.filter{it.tobreak == true}.each { e->
    //Erase memory of last coop action across broken tie
    def v1 = e.getVertex(Direction.IN)
    def v2 = e.getVertex(Direction.OUT)
    v1.private["coop_" + v2.id] = 0
    v1["coop_" + v2.id] = 0
    v2.private["coop_" + v1.id] = 0
    v2["coop_" + v1.id] = 0
    g.removeEdge(e)  
  }
  
  if(dynamic){
    makeTiesStep.start()
  }else{
    if (curRound < nRounds) {
      curRound++
      cooperationStep.start()
    } else {
      finishedStep.start()
    }
  }
}
