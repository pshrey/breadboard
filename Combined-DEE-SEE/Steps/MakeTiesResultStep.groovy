makeTiesResultStep = stepFactory.createStep()

makeTiesResultStep.run = {
  println "makeTiesResultStep.run: ${curRound}"
  a.addEvent("StepStart", ["step":"makeTiesResultStep","curRound":curRound])
  curStep = "makeTiesResult"

//new 
if (curRound %3 == 0) {
  g.E.each { e->
    if (e.tomake) {
      e.bothV.each { v->
        e.private(v, [arrow:"both"]) 
      }
      e.tomake = false
    }
    
    if (e.tobreak) {
      e.bothV.each { v->
        e.private(v, [breaking:1]) 
      }
      e.breaking = 1
    }
  }
  
  g.V.filter{it.active}.each { v->
    v.text = c.get("MakeTiesResultStep", v.offerAcceptTies, v.offerRejectTies, v.acceptOfferTies, v.rejectOfferTies)
    a.add(v, [name: "Next", result: {
      v.text += "<p>Please wait for the other players to click 'Next'.</p>"
    }])
  }
}

  
  
} //new curRound

makeTiesResultStep.done = {
  println "makeTiesResultStep.done"
  g.E.filter{it.tobreak == true}.each { e->
    g.removeEdge(e)
  }
  
  if (curRound < nRounds) {
    curRound++
    cooperationStep.start()
  } else {
    finishedStep.start()
  }
}
