makeTiesStep = stepFactory.createStep()

makeTiesStep.run = {
  println "makeTiesStep.run: ${curRound}"
  a.addEvent("StepStart", ["step":"makeTiesStep","curRound":curRound])
  curStep = "makeTies"
  
  
//new 
if (curRound%3 == 0) {
  g.V.filter{it.active == true}.each { v->
    v.toConfirm = [] 
    v.offerAcceptTies = 0 // ego offer, alter accept
    v.offerRejectTies = 0 // ego offer, alter reject
    v.acceptOfferTies = 0 // ego accept, alter offer
    v.rejectOfferTies = 0 // ego reject, alter offer
  }
  
  def reputation="(Average amount each of their neighbors received from them)"
  if(inAndOutGroupRep&&politicalOrientation){
    reputation="(Average amount each of their same party neighbors received from them)(Average amount each of their opposite party neighbors received from them)"
  }else if(inGroupRep&&politicalOrientation){
    reputation="(Average amount each of their same party neighbors received from them)"
  }
  g.V.filter{it.active == true}.each { v->
    // Only players who cut ties get to make new ones
    if (v.private.cutCount > 0) {
      v.text = c.get("MakeTiesStep", reputation)
      def choices = []
      g.V.filter{it.active == true}.each { n->
        if (v.id != n.id && (! g.hasEdge(v, n)) ) {
          def coopRate = getCoopRate(v, n)         
          // commenting this and replacing with something that only works with treatment=1
          // def label = coopRate.score
          //HJ changes
          def label=""
          //avoid divide by zero issues
          if(n.private.numInGroupNeighbors==0){
            n.private.numInGroupNeighbors=1
          }
          if(n.private.numOutGroupNeighbors==0){
            n.private.numOutGroupNeighbors=1
          }
          
          if(inAndOutGroupRep&&politicalOrientation){
            //avg ingroup and out group last three rounds
            label = n.score.substring(0,1) + ": ${n.private.score} (${n.private.neighborReceived3IN/n.private.numInGroupNeighbors}) (${n.private.neighborReceived3OUT/n.private.numOutGroupNeighbors})" 
          }else if(inGroupRep&&politicalOrientation){
            //avg in group last three rounds
            label = n.score.substring(0,1) + ": ${n.private.score} (${n.private.neighborReceived3IN/n.private.numInGroupNeighbors})" 
          }else{
            //avg last three rounds
            label = n.score.substring(0,1) + ": ${n.private.score} (${n.private.neighborReceived3/3})" 
          }
          //End of HJ changes
          def choice = [name: label,
                        class: coopRate.color,
                        event: [name:"MakeTie", data:[pid:v.id, nid:n.id]],
                        result: {
                          n.toConfirm << v
                          v.text += c.get("Wait")
                        }]
          choices << choice
        } // if (v.id != n.id...
      } // g.V.each { n->
      a.add(v, *choices)
    } else {
      v.text = c.get("SkipMakeTiesStep")
    }
  } // g.V.filter{it.active == true && it.cutTieCount > 0}.each 
  
  //HJ reset all values used to calculate reputation
  g.V.filter{it.active == true}.each { v->
    v.neighborRecived3=0
    v.private.neighborReceived3IN=0 
    v.private.numInGroupNeighbors=0 
    v.private.neighborReceived3OUT=0
    v.private.numOutGroupNeighbors=0
  }
  
} //if 3 rounds passed 
  
} // makeTiesStep.run
  
makeTiesStep.done = {
  println "makeTiesStep.done"
  confirmTiesStep.start()
}
