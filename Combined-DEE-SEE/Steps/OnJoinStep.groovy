onJoinStep = stepFactory.createNoUserActionStep()

onJoinStep.run = { playerId->
  //println "onJoinStep.run"
  def v = g.getVertex(playerId)
  //v.private.score = initScore
  v.private.totalCoopCount = 0 // # player coop choices
  v.private.totalDefectCount = 0 // # player defect choices
  v.private.coopCount = 0 // # neighbors who the player cooperated with last round
  v.private.defectCount = 0 // # neighbors who the player defected with last round
  v.private.nCoopCount = 0 // # neighbors who cooperated last round
  v.private.nDefectCount = 0 // # neighbors who defected last round
  
  
  v.private.cutCount = 0 // # ties you cut last round
  v.private.nCutCount = 0 // # ties your neighbors cut with you last round
  
  v.toConfirm = [] // List of players who are offering to make a tie with you
  
  v.correctAnswers = 0 // Number of answers correctly answered in comprehension test
  v.active = false
  v.dropped = false
  
  //Testing if skip conditional is met
  v.private.wasSkipped = 0 
  
  // AI players skip the tutorial
  if (v.ai != 1) {
    v.private.ai = 0
    //commenting for testing 
    
    //Testing if skip conditional is met
    v.private.wasSkipped = 1 
    
    // Show start at timer
    //def timeToStart = 180
    def curTime = new Date().getTime()
    if (startAt == null) {
          startAt = curTime + 30000 //wait time in milliseconds.
    }
    
    def timeToStart = ((startAt - curTime)/1000).toInteger()
    
    /*
    if (startAt != null) {
      println("startAt: " + startAt)
      timeToStart = ((int)(startAt - (new Date().getTime())))/1000
    }
    
    
    g.addTimer(
      time: timeToStart,
      timerText: "The game will begin in: ",
      direction: "down",
      type: "time",
      currencyAmount: "0",
      result: {},
      player: v,
      appearance: "info"
    )
    */
    //Above /*---- */ shoud stay commentated 
    
    //more commenting for testing 
    
    
    // add initStep.start in results closure of timer to make timer determine when initStep starts instead of button on back end
    println("timeToStart: " + timeToStart)
    g.addTimer(timeToStart, v.id + "_gameStartTimer", "The game will begin in approximately: ", "down", "time", "0", {
    	initStep.start()
    }, v, "info")
    
    if (!skip) {
    v.text = c.get("ConsentPage1")
    a.add(v, [name: "Next", result: {
      v.text = c.get("ConsentPage2")
    }])
    a.add(v, [name: "Continue", result: {
      v.text = c.get("Tutorial1", "some initial amount of ")
    }])
    a.add(v, [name: "Next", result: {
      if (socialcapital) {
        v.text = c.get("Tutorial2Group")
      } else {
        v.text = c.get("Tutorial2")
      }
    }])
    a.add(v, [name: "Next", result: {
      if (dynamic) {
      	v.text = c.get("Tutorial3")
      } else {
        v.text = c.get("TutorialStatic")
      }
    }])
    if (dynamic) {
      a.add(v, [name: "Next", result: {
        v.text = c.get("Tutorial4")
      }])
    }
    def tutorialFourFill
    switch(treatment) {
      case 1:
        tutorialFourFill = c.get("NoInformationCondition")
        break
      case 2:
        tutorialFourFill = c.get("OneLinkRemovedCondition", ((showPreviousRound) ? "<strong>on the previous round only</strong>" : "<strong>over the entire session</strong>"), coc, po)
        break
      case 3:
        tutorialFourFill = c.get("FullInformationCondition", ((showPreviousRound) ? "<strong>on the previous round only</strong>" : "<strong>over the entire session</strong>"), coc, po) 
        break
    }
    }
    
    
    //political orientation question
    
    //next button takes us to the question page.
    if (politicalOrientation) {
      a.add(v, [name:"Next", result: { v.text = c.get("PoliticalOrientationQ")}]) //pose political question.

      //display answer options to the user.
       a.add(v, 
          [
            name:"1) Much closer to the Republican Party", 
            result: 
            { 
              v.polOr = 1
              v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")
            }
          ],
          [
            name:"2) Closer to the Republican Party", 
            result: 
            { 
              v.polOr = 2
              v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")
            }
          ],
          [
            name:"3) Somewhat closer to the Republican Party", 
            result: 
            {
              v.polOr = 3
              v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")
            }
          ],
          [
            name:"4) Somewhat closer to the Democratic Party", 
            result: 
            {
              v.polOr = 4
              v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")
            }

          ],
          [
            name:"5) Closer to the Democratic Party", 
            result: 
            {
              v.polOr = 5
              v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")
            }
          ],
          [
            name:"6) Much Closer to the Democratic Party", 
            result: 
            {
              v.polOr = 6
              v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")
            }
          ])
    }
    
    if (skip) { //Skips comprehension question oif. theparameter 'skip' is set to true.
    	a.add(v, { v.text = c.get("EndTest") }, [name: "Next", result: {
       	 v.active = true
         v.correctAnswers = 4
       	 v.text = c.get("PleaseWait")
      	}])
    } else {
      //Comprehension Question 1

      //a.add(v, [name:"Next", result: { v.text = c.get("ComprehensionTest" ) +c.get("ComprehensionTestQ1")}])
      // adding array to randomize answer prompts 
      oneThroughFour = [1, 2, 3, 4]


      //display four possible question responses in random order
      Collections.shuffle(oneThroughFour)
      if  (oneThroughFour[0] == 1){
        a.add(v, 
          [
            name:"You pay 30 points and the neighbor gets points based on how many points you have.", 
            result: 
            { 
              if (socialcapital) {
                v.correctAnswers+= 1                                                                                         
                v.text = c.get("AnswerCorrect") 
              }else{
                v.text = c.get("ComprehensionTestQ1Incorrect")
              }
            }
          ],
          [
            name:"You pay 30 points for the neighbor to gain 60 points.", 
            result: 
            { 
              if (!socialcapital) {
                v.correctAnswers+= 1
                v.text = c.get("AnswerCorrect")
              }else{
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }
            }
          ],
          [
            name:"Your points do not change and the neighbor gains 60 points.", 
            result: 
            {
              if(socialcapital){
               v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
               v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ],
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              if(socialcapital){
                v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
                v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ])
      }else if(oneThroughFour[0] == 3){
        a.add(v, 
          [
            name:"Your points do not change and the neighbor gains 60 points.", 
            result: 
            {
              if(socialcapital){
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
                  v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ],
          [
            name:"You pay 30 points and the neighbor gets points based on how many points you have.", 
            result: 
            {
              if (socialcapital) {
                v.correctAnswers+= 1                                                                                         
                v.text = c.get("AnswerCorrect")
              }else{
                v.text = c.get("ComprehensionTestQ1Incorrect")
              }
            }
          ],
          [
            name:"You pay 30 points for the neighbor to gain 60 points.", 
            result: 
            { 
              if (!socialcapital) {
                v.correctAnswers+= 1
                v.text = c.get("AnswerCorrect")
              }else{
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }
            }
          ],
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              if(socialcapital){
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
                  v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ])
      }else if(oneThroughFour[0] == 2){
        a.add(v, 
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              if(socialcapital){
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
                  v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ],
          [
            name:"Your points do not change and the neighbor gains 60 points.", 
            result: 
            {
              if(socialcapital){
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
                  v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ],
          [
            name:"You pay 30 points and the neighbor gets points based on how many points you have.", 
            result: 
            { 
              if (socialcapital) {
                v.correctAnswers+= 1                                                                                         
                v.text = c.get("AnswerCorrect")
              }else{
                v.text = c.get("ComprehensionTestQ1Incorrect")
              }
            }
          ],
          [
            name:"You pay 30 points for the neighbor to gain 60 points.", 
            result: 
            { 
              if (!socialcapital) {
                v.correctAnswers+= 1
                v.text = c.get("AnswerCorrect")
              }else{
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }
            }
          ])
      }else {
        a.add(v, 
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              if(socialcapital){
               v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
               v.text = c.get("ComprehensionTestQ1Incorrect") 
               }
            }
          ],
          [
            name:"You pay 30 points for the neighbor to gain 60 points.", 
            result: 
            { 
              if (!socialcapital) {
                v.correctAnswers+= 1
                v.text = c.get("AnswerCorrect")
              }else{
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }
            }
          ],
          [
            name:"You pay 30 points and the neighbor gets points based on how many points you have.", 
            result: 
            { 
              if (socialcapital) {
                v.correctAnswers+= 1                                                                                         
                v.text = c.get("AnswerCorrect")
              }else{
                v.text = c.get("ComprehensionTestQ1Incorrect")
              }
            }
          ],
          [
            name:"Your points do not change and the neighbor gains 60 points.", 
            result: 
            {
              if(socialcapital){
                  v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
              }else{
                  v.text = c.get("ComprehensionTestQ1Incorrect") 
              }
            }
          ])
       }

      /*if(v.correctAnswers != 1){
        if(socialcapital){
         v.text = c.get("ComprehensionTestQ1IncorrectSocialCapital") 
        }else{
         v.text = c.get("ComprehensionTestQ1Incorrect") 
        }
        a.add(v, [name:"Next1", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ2") }])
      }*/


      //Comprehension question 2
      a.add(v, [name:"Next", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ2") }])


      //prompting 4 questions in random order
      Collections.shuffle(oneThroughFour)
      if  (oneThroughFour[0] == 1){
        a.add(v, 
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect")
            }
          ],
          [
            name:"You pay 50 points for the neighbor to gain 100 points.", 
            result: 
            {
              v.text = c.get("ComprehensionTestQ2Incorrect") 
            }
          ],
          [
            name:"You pay 100 points for the neighbor to gain 100 points.", 
            result: 
            {
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"Your points do not change and the neighbor gains 100 points.", 
            result: 
            {
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ])
      }else if(oneThroughFour[0] == 3){
        a.add(v, 
          [
            name:"You pay 100 points for the neighbor to gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect")
            }
          ],
          [
            name:"You pay 50 points for the neighbor to gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"Your points do not change and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ])
      }else if(oneThroughFour[0] == 2){
        a.add(v, 
          [
            name:"Your points do not change and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"You pay 100 points for the neighbor to gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect") 
            }
          ],
          [
            name:"You pay 50 points for the neighbor to gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ])
      }else {
        a.add(v, 
          [
            name:"Your points do not change and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"You pay 50 points for the neighbor to gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ],
          [
            name:"Your points and the neighbor's points do not change.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect")
            }
          ],
          [
            name:"You pay 100 points for the neighbor to gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ2Incorrect")  
            }
          ])      	
      }

      //Comprehension question 4 – the third to be asked in current implementation.
      a.add(v, [name:"Next", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ4") }])

      //prompting 4 responses in random order
      Collections.shuffle(oneThroughFour)
      if  (oneThroughFour[0] == 1){
        a.add(v, 
          [
            name:"You and the neighbor both gain 0 points.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect")
            }
          ],
          [
            name:"You and the neighbor both gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")
            }
          ],
          [
            name:"You lose 50 points and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")
            }
          ],
          [
            name:"You gain 100 points and the neighbor loses 50 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")
            }
          ])
      }else if(oneThroughFour[0] == 3){
        a.add(v, 
          [
            name:"You lose 50 points and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect") 
            }
          ],
          [
            name:"You and the neighbor both gain 0 points.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect") 
            }
          ],
          [
            name:"You and the neighbor both gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")
            }
          ],
          [
            name:"You gain 100 points and the neighbor loses 50 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")  
            }
          ])
      }else if(oneThroughFour[0] == 2){
        a.add(v, 
          [
            name:"You gain 100 points and the neighbor loses 50 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect") 
            }
          ],
          [
            name:"You lose 50 points and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect") 
            }
          ],
          [
            name:"You and the neighbor both gain 0 points.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect")
            }
          ],
          [
            name:"You and the neighbor both gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")
            }
          ])
      }else {
        a.add(v, 
          [
            name:"You gain 100 points and the neighbor loses 50 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect") 
            }
          ],
          [
            name:"You and the neighbor both gain 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect")
            }
          ],
          [
            name:"You and the neighbor both gain 0 points.", 
            result: 
            {
              v.correctAnswers+= 1 
              v.text = c.get("AnswerCorrect") 
            }
          ],
          [
            name:"You lose 50 points and the neighbor gains 100 points.", 
            result: 
            { 
              v.text = c.get("ComprehensionTestQ4Incorrect") 
            }
          ])      	
      }      

      //Comprehension question 5 – the fourth question to be asked in current version.
      a.add(v, [name:"Next", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ5") }])

      // Skip question for no information condition

      if (treatment > 1) {
        if (showPreviousRound) {
          //prompting 4 questions in random order

          Collections.shuffle(oneThroughFour)
          if  (oneThroughFour[0] == 1){
            a.add(v, [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                         v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }])
          }else if(oneThroughFour[0] == 3){
            a.add(v, [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1} 
                                                                                                  v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }])
          }else if(oneThroughFour[0] == 2){
            a.add(v, [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                                  v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }])
          }else {
            a.add(v, [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }],
            [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                         v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Prev") }])
          }

          //Q6Prev answers here

          Collections.shuffle(oneThroughFour)
          if  (oneThroughFour[0] == 1){
            a.add(v, [name:"The percent of the time the neighbor chose Give in the previous round", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }],
            [name:"The percent of the time the neighbor chose Give over the entire session", result: { v.text = c.get("ComprehensionTest") }])
          }else if(oneThroughFour[0] == 3){
            a.add(v, [name:"The percent of the time the neighbor chose Give over the entire session", result: { v.text = c.get("ComprehensionTest")}],
            [name:"The percent of the time the neighbor chose Give</strong> in the previous round", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }])
          }else if(oneThroughFour[0] == 2){
            a.add(v, [name:"The percent of the time the neighbor chose Give in the previous round", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }],
            [name:"The percent of the time the neighbor chose Give</strong> over the entire session", result: { v.text = c.get("ComprehensionTest") }])
          }else {
            a.add(v, [name:"The percent of the time the neighbor chose Give over the entire session", result: { v.text = c.get("ComprehensionTest") }],
            [name:"The percent of the time the neighbor chose Give in the previous round", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }])
          } 

        } else {
          //prompting 4 questions in random order
          Collections.shuffle(oneThroughFour)
          if  (oneThroughFour[0] == 1){
            a.add(v, [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1}  
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                         v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }])
          }else if(oneThroughFour[0] == 3){
            a.add(v, [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                                  v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Always decide whether or not you want to stop interacting with one of the otherr neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }])
          }else if(oneThroughFour[0] == 2){
            a.add(v, [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                                  v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }])
          }else {
            a.add(v, [name:"Always decide whether or not you want to stop interacting with one of your neighbors.", result: { v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", result: {if (dynamic) {v.correctAnswers+= 1} 
                                        v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }],
            [name:"Always begin a new round of decisions with the same neighbors.", result: {if (!dynamic) {v.correctAnswers+= 1}
                                                                                         v.text = c.get("ComprehensionTest")  + c.get("ComprehensionTestQ6Ent") }])
          }
          //Q6 Ent Answers here
          Collections.shuffle(oneThroughFour)
          if  (oneThroughFour[0] == 1){
            a.add(v, [name:"The percent of the time the neighbor chose Give over the entire session", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }],
            [name:"The percent of the time the neighbor chose Give in the previous round", result: { v.text = c.get("ComprehensionTest") }])
          }else if(oneThroughFour[0] == 3){
            a.add(v, [name:"The percent of the time the neighbor chose <strong class='a'>Give in the previous round", result: { v.text = c.get("ComprehensionTest")}],
            [name:"The percent of the time the neighbor chose Give over the entire session", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }])
          }else if(oneThroughFour[0] == 2){
            a.add(v, [name:"The percent of the time the neighbor chose Give over the entire session", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }],
            [name:"The percent of the time the neighbor chose Give in the previous round", result: { v.text = c.get("ComprehensionTest") }])
          }else {
            a.add(v, [name:"The percent of the time the neighbor chose Give in the previous round", result: { v.text = c.get("ComprehensionTest") }],
            [name:"The percent of the time the neighbor chose Give over the entire session", result: {v.correctAnswers+= 1 
                                        v.text = c.get("ComprehensionTest") }])
          }
        }

      }else{
        //Q5 again
        Collections.shuffle(oneThroughFour)
        if  (oneThroughFour[0] == 1){
          a.add(v, 
            [
              name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                } 
              }
            ],
            [
              name:"Always decide whether or not you want to stop interacting with one of your neighbors.", 
              result: 
              { 
                if (dynamic) {
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                } 
              }
            ],
            [
              name:"Always begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (!dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }
              }
            ])
        }else if(oneThroughFour[0] == 3){
          a.add(v, 
            [
              name:"Always begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (!dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }
              }
            ],
            [
              name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                }
              }
            ],
            [
              name:"Always decide whether or not you want to stop interacting with one of your neighbors.", 
              result: 
              { 
                if (dynamic) {
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                } 
              }
            ])
        }else if(oneThroughFour[0] == 2){
          a.add(v, 
            [
              name:"Always begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (!dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }
              }
            ],
            [
              name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                }
              } 
            ],
            [
              name:"Always decide whether or not you want to stop interacting with one of your neighbors.", 
              result: 
              {
                if (dynamic) {
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                }
              }
            ])
        }else {
          a.add(v, 
            [
              name:"Always decide whether or not you want to stop interacting with one of your neighbors.", 
              result: 
              { 
                if (dynamic) {
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                }
              }
            ],
            [
              name:"Periodically decide whether or not you want to stop interacting with one of your neighbors; otherwise, begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectStatic")
                } 
              }
            ],
            [
              name:"Always begin a new round of decisions with the same neighbors.", 
              result: 
              {
                if (!dynamic) {
                  v.correctAnswers+= 1
                  v.text = c.get("AnswerCorrect")
                }else{
                  v.text = c.get("ComprehensionTestQ5IncorrectDynamic")
                }
              }
            ])
        }         
      }
    
    
      a.add(v, [name:"Next", result: { v.text = c.get("ComprehensionTest") }])

      a.add(v, { v.text = c.get("EndTest") }, [name: "Next", result: {
        if (v.correctAnswers >= testPassScore) {
          v.active = true
          v.text = c.get("PleaseWait")

        } else {
          a.remove(v)
          v.text = c.get("FailedTest", g.getSubmitForm(v, "0", "failed_test", amtSandbox, true))
        }
      }])
    }
    
    //ignore quiz for testing 
    //v.active = true
  } else {
    // Set AI players active
    v.active = true
    v.private.wasSkipped = 2
    if (politicalOrientation) { //randomize political orientation for AI players
    	v.polOr = r.nextInt(6) + 1
    }
  }
}
onJoinStep.done = {
  //println "onJoinStep.done"
}

