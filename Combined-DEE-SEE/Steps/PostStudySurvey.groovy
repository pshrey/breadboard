q7 = [id: "q7",
      name: "r_gender",
      text: "What is your sex?",
      qtype: "radio",
      options: [
       [value: "male", text: "Male"],
       [value: "female", text: "Female"]
      ]
     ]

q8 = [id: "q8",
      name: "r_ethnicity",
      text: "Please select which race / ethnicity you most identify as.",
      qtype: "radio",
      options: [
       [value: "white", text: "White / Caucasian"],
       [value: "black", text: "Black / African-American"],
       [value: "latino", text: "Latino / Hispanic"],
       [value: "asian", text: "Asian / Asian-American"],
       [value: "other", text: "Other"]
      ]
     ]

q9 = [id: "q9",
      name: "r_age",
      text: "What is your age?",
      qtype: "numeric"
     ]

q10 = [id: "q10",
      name: "r_location",
      text: "If you live in the U.S., which state do you live in?",
      qtype: "select",
      options: [
        [value: "outside_us", text: "I do not live in the U.S."],
        [value: "AL", text: "Alabama"],
        [value: "AK", text: "Alaska"],
        [value: "AZ", text: "Arizona"],
        [value: "AR", text: "Arkansas"],
        [value: "CA", text: "California"],
        [value: "CO", text: "Colorado"],
        [value: "CT", text: "Connecticut"],
        [value: "DE", text: "Delaware"],
        [value: "DC", text: "District of Columbia"],
        [value: "FL", text: "Florida"],
        [value: "GA", text: "Georgia"],
        [value: "HI", text: "Hawaii"],
        [value: "ID", text: "Idaho"],
        [value: "IL", text: "Illinois"],
        [value: "IN", text: "Indiana"],
        [value: "IA", text: "Iowa"],
        [value: "KS", text: "Kansas"],
        [value: "KY", text: "Kentucky"],
        [value: "LA", text: "Louisiana"],
        [value: "ME", text: "Maine"],
        [value: "MD", text: "Maryland"],
        [value: "MA", text: "Massachusetts"],
        [value: "MI", text: "Michigan"],
        [value: "MN", text: "Minnesota"],
        [value: "MS", text: "Mississippi"],
        [value: "MO", text: "Missouri"],
        [value: "MT", text: "Montana"],
        [value: "NE", text: "Nebraska"],
        [value: "NV", text: "Nevada"],
        [value: "NH", text: "New Hampshire"],
        [value: "NJ", text: "New Jersey"],
        [value: "NM", text: "New Mexico"],
        [value: "NY", text: "New York"],
        [value: "NC", text: "North Carolina"],
        [value: "ND", text: "North Dakota"],
        [value: "OH", text: "Ohio"],
        [value: "OK", text: "Oklahoma"],
        [value: "OR", text: "Oregon"],
        [value: "PA", text: "Pennsylvania"],
        [value: "RI", text: "Rhode Island"],
        [value: "SC", text: "South Carolina"],
        [value: "SD", text: "South Dakota"],
        [value: "TN", text: "Tennessee"],
        [value: "TX", text: "Texas"],
        [value: "UT", text: "Utah"],
        [value: "VT", text: "Vermont"],
        [value: "VA", text: "Virginia"],
        [value: "WA", text: "Washington"],
        [value: "WV", text: "West Virginia"],
        [value: "WI", text: "Wisconsin"],
        [value: "WY", text: "Wyoming"],
        [value: "other", text: "Other"]
      ]
     ]


q11 = [id: "q11",
      name: "r_education",
      text: "Which of the following best describes your highest level of education completed?",
      qtype: "radio",
      options: [
       [value: "lt_high_school", text: "Less than high school"],
       [value: "high_school", text: "High school degree / GED"],
       [value: "some_college", text: "Some college"],
       [value: "associates_degree", text: "Associate's degree"],
       [value: "bachelors_degree", text: "Bachelor's degree"],
       [value: "graduate_degree", text: "Graduate degree"]
      ]
     ]

q12 = [id: "q12",
      name: "r_participantID",
      text: "What is your unique worker ID (i.e., Participant ID)?",
      qtype: "text",
      options: [
      ]
     ]
q13 = [id: "q13",
      name: "r_questionInstruction",
      text: """
            In this set of questions, we ask you to imagine that you have been randomly paired with another person, whom we will refer to simply as the "other."?
            Other is someone you do not know and that you will not knowingly meet in the future. Both you and Other will be making choices by selecting either the letter A, B, or C.
            Your own choices will produce points for yourself and Other. Likewise, Other’s choice will produce points for him/her and for you. 
            Every point has value: The more points you receive, the better for you, and the more points Other receives, the better for him/her. 
            <br><br>Here’s an example of how this task works.<br>
            <table style='width: 100%; background: white; text-align: center'>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'></td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>A</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>B</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>C</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>You Get</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>550</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>Other Gets</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>100</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>300</td>
              </tr>
            </table>
            <br>
            In this example, if you chose A you would receive 500 points and Other would receive 100 points; if you chose B, you would receive 500 points and Other 500; and if you chose C, you would receive 550 points and Other 300. 
            So, you see that your choice influences both the number of points you receive and the number of points the other receives. 
            <br><br>Before you begin making choices, keep in mind that there are no right or wrong answers – choose the option that you, for whatever reason, prefer most. 
            Also, remember that the points have value: The more of them you accumulate, the better for you. 
            Likewise, from the Other’s point of view, the more points s/he accumulates, the better for him/her.""",
	  qtype: "instruction",
      options: [
       ]
      ]

q14 = [id: "q14",
       name: "r_tableq1",
       text: """
       For the choice situation below, select  <u>A, B or C</u>, depending on which column you prefer most.
       <br><br>
       <table style='width: 100%; background: white; text-align: center'>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'></td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>A</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>B</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>C</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>You Get</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>480</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>540</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>480</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>Other Gets</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>80</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>280</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>480</td>
              </tr>
       </table>
       """,
       qtype: "radio",
       options: [
         	[value: "a", text: "Column A"],
         	[value: "b", text: "Column B"],
         	[value: "c", text: "Column C"]
         ]
       ]

q15 = [id: "q15",
       name: "r_tableq2",
       text: """
       For the choice situation below, select  <u>A, B or C</u>, depending on which column you prefer most.
       <br><br>
       <table style='width: 100%; background: white; text-align: center'>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'></td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>A</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>B</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>C</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>You Get</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>560</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>Other Gets</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>300</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>100</td>
              </tr>
       </table>
       """,
       qtype: "radio",
       options: [
         	[value: "a", text: "Column A"],
         	[value: "b", text: "Column B"],
         	[value: "c", text: "Column C"]
         ]
       ]

q16 = [id: "q16",
       name: "r_tableq3",
       text: """
       For the choice situation below, select  <u>A, B or C</u>, depending on which column you prefer most.
       <br><br>
       <table style='width: 100%; background: white; text-align: center'>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'></td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>A</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>B</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>C</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>You Get</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>520</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>520</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>580</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>Other Gets</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>520</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>120</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>320</td>
              </tr>
       </table>
       """,
       qtype: "radio",
       options: [
         	[value: "a", text: "Column A"],
         	[value: "b", text: "Column B"],
         	[value: "c", text: "Column C"]
         ]
       ]
q17 = [id: "q17",
       name: "r_tableq4",
       text: """
       For the choice situation below, select  <u>A, B or C</u>, depending on which column you prefer most.
       <br><br>
       <table style='width: 100%; background: white; text-align: center'>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'></td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>A</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>B</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>C</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>You Get</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>560</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>490</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>Other Gets</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>100</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>300</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>490</td>
              </tr>
       </table>
       """,
       qtype: "radio",
       options: [
         	[value: "a", text: "Column A"],
         	[value: "b", text: "Column B"],
         	[value: "c", text: "Column C"]
         ]
       ]

q18 = [id: "q18",
       name: "r_tableq5",
       text: """
       For the choice situation below, select  <u>A, B or C</u>, depending on which column you prefer most.
       <br><br>
       <table style='width: 100%; background: white; text-align: center'>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'></td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>A</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>B</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>C</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>You Get</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>560</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>490</td>
              </tr>
              <tr>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>Other Gets</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>300</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>500</td>
                <td style='width: 25%; border: 1px solid black; padding: 0.5em;'>90</td>
              </tr>
       </table>
       """,
       qtype: "radio",
       options: [
         	[value: "a", text: "Column A"],
         	[value: "b", text: "Column B"],
         	[value: "c", text: "Column C"]
         ]
       ]
q19 = [id: "q19",
       name: "r_comments",
       text: "",
       qtype: "textArea",
       options: [
         ]
       ]


/* Commenting the trust questions, if needed later
q12 = [id: "q12",
      name: "people_trusted",
      text: "Generally speaking, would you say that most people can be trusted, or that you can't be too careful in dealing with people?",
      qtype: "radio",
      options: [
       [value: "can_be_trusted", text: "Most people can be trusted"],
       [value: "cant_be_too_careful", text: "You can't be too careful"]
      ]
     ]

q13 = [id: "q13",
      name: "people_helpful",
      text: "Would you say that most of the time, people try to be helpful, or that they are mostly just looking out for themselves?",
      qtype: "radio",
      options: [
       [value: "try_to_be_helpful", text: "Try to be helpful"],
       [value: "look_out_for_themselves", text: "Look out for themselves"]
      ]
     ]

q14 = [id: "q14",
      name: "people_take_advantage",
      text: "Do you think that most people would try to take advantage of you if they got the chance, or would they try to be fair?",
      qtype: "radio",
      options: [
       [value: "take_advantage", text: "Take advantage"],
       [value: "try_to_be_fair", text: "Try to be fair"]
      ]
     ]
*/




def createSurveyQuestion(q, player, randomize=false) {
  
  if ( (q.qtype == "radio" || q.qtype == "checkbox" || q.qtype == "select") && randomize ) {
    Collections.shuffle(q.options)
  }
  
  def choiceHtml = ""
  
  switch (q.qtype) {
    case "radio":
      q.options.each { option->
        choiceHtml += """
          <input type="radio" name="${q.name}" class="param" ng-model="${q.name}" value="${option.value}" required>
          <label for="${option.value}"> ${option.text}</label><br>  
        """
      }
      break
    case "select":
      def optionHtml = ""
      q.options.each { option->
        optionHtml += """<option value="${option.value}">${option.text}</option>"""
      }
      
      choiceHtml += """
        <select name="${q.name}" class="param" ng-model="${q.name}" required>
          ${optionHtml}
        </select>
      """
      break
    case "numeric":
      choiceHtml += """
        <input type="number" name="${q.name}" class="param" ng-model="${q.name}" min="16" max="120" required>
      """
      break
    case "text":
      choiceHtml += """
        <input type="text" name="${q.name}" class="param" ng-model="${q.name}" required>
      """
      break
    case "textArea":
      choiceHtml += """
      	<textarea name="${q.name}" class="param" ng-model="${q.name}" rows="5" cols="50" spellcheck="false" style="margin: 0px; height: 179px; width: 362px;"></textarea>
      """
  }
  def output = ""
  if(q.qtype == "instruction") {
    output = """
              <p>${q.text}</p>
              <div>
                ${choiceHtml}
              </div>
            """.toString()
  }else{
    output = """
              <p><strong>${q.text}</strong></p>
              <div>
                ${choiceHtml}
              </div>
            """.toString()
  }
  
  if(q.id == "q19"){
    	player.text = ""
        def dollars = (player.private.score / 1000)
        output = """
                  <p>${c.get("Finished", currency.format(dollars))}</p>
                  <div>
                    ${choiceHtml}
                  </div>
       			""".toString()
      }
  return  [name: (q.id == "q19") ? "Complete" : "Next",
           custom: output,
    
    result: { params->
      if (params != null) {
        println(params)
        a.addEvent(q.id, 
          ["pid": player.id, 
           (q.name) : params[q.name]])
        log("Player " + player.score.substring(0,1) + " Survey " + q.name + ": " + params[q.name], experimentData)
      }
      if(q.id == "q19"){
        player.text = "Thank you for completing this study. The link for Prolific is: https://app.prolific.co/submissions/complete?cc=30582056, or you may manually enter 30582056."
      }
      /*if(q.id == "q18"){
        def dollars = (player.private.score / 1000)
        player.text = c.get("Finished", currency.format(dollars), g.getSubmitForm(player, dollars, "completed", amtSandbox, true))
      }*/
    }]
}