cooperationStep = stepFactory.createStep()

cooperationStep.run = {
  println "cooperationStep.run: ${curRound}"
  a.addEvent("StepStart", ["step":"cooperationStep","curRound":curRound])
  curStep = "cooperation"
  
  //removes edges from dropped players 
  g.V.filter{!it.active}.each {v->
    g.removeEdges(v)}
  
  // Drop players who have no ties 
  g.V.filter{it.active}.each { v->
    if (v.neighbors.count() == 0) {
      log("Player " + v.score + " was dropped due to not having any neighbors", experimentData)
      v.text = c.get("NoEdges") + c.get("PostStudySurvey")
      
      a.add(v, createSurveyQuestion(q7, v))
      a.add(v, createSurveyQuestion(q8, v))
      a.add(v, createSurveyQuestion(q9, v))
      a.add(v, createSurveyQuestion(q10, v))
      a.add(v, createSurveyQuestion(q11, v))
      a.add(v, createSurveyQuestion(q12, v))
      a.add(v, createSurveyQuestion(q13, v))
      a.add(v, createSurveyQuestion(q14, v))
      a.add(v, createSurveyQuestion(q15, v))
      a.add(v, createSurveyQuestion(q16, v))
      a.add(v, createSurveyQuestion(q17, v))
      a.add(v, createSurveyQuestion(q18, v))
      a.add(v, createSurveyQuestion(q19, v))
      
     /* a.add(v, [name: "Next", result: {
        a.addEvent("FinalScore", [pid:v.id, score:v.private.score])
        def dollars = (v.private.score / 1000)
        v.text = c.get("Finished", currency.format(dollars), g.getSubmitForm(v, dollars, "droppedNoEdges", amtSandbox, true))
      }])*/
        
      v.active = false
    }
  }
  
  // Reset coopCount
  g.E.each { e->
    e.coopcount = 0
  }
  
  g.V.filter{it.active}.each { v->

    // Declare cooperation counts
    v.private.coopCount = 0 
    v.private.defectCount = 0 
    v.private.nCoopCount = 0 
    v.private.nDefectCount = 0
    v.private.earned = 0
    v.private.remember = 0
    v.private.give=0
    v.private.paid=0
    v.private.neighborReceive = 0 // holds the value of the points the neighbors receive
    v.private.payment = 0 // for keeping track of the money you paid to give points to the neighbors
    //HJ added
    v.private.resultMessage="" //for displaying how much player gives in individual choice 
    
    //the following are reset in make ties step
    v.private.neighborReceived3 = 0 // holds the value of the points the neighbors received over last 3 rounds
    v.private.neighborReceived3IN=0 // holds the value of the points the in group neighbors received over last 3 rounds 
    v.private.numInGroupNeighbors=0 //num in group neighbors over last three rounds
    v.private.neighborReceived3OUT=0// holds the value of the points the out group neighbors received over last 3 rounds 
    v.private.numOutGroupNeighbors=0//num out group neighbors over last three rounds
    //end of HJ added
  }
  
  
  g.V.filter{it.active}.each { v->

    // Reset cooperation counts
    v.private.coopCount = 0 
    v.private.defectCount = 0 
    v.private.nCoopCount = 0 
    v.private.nDefectCount = 0
    v.private.earned = 0
    v.private.remember = 0
    v.private.give=0
    v.private.neighborReceive = 0
    v.private.payment = 0
    //HJ added
    v.private.resultMessage="" 
    //end of HJ added
    
    
    v.bothE.each { e->
      e.private(v, [current:"-1", arrow: ""])
    }
    
    if (individualChoice) { //lots of changes HJ
      
      v.neighbors.each { n->
        v.private.pay = v.private.remember
        v.private.give = 0
        def curEdge = g.getEdge(v, n)
        def init = {
          // When initializing each queued playerAction, mark the edge as current and add an arrow.
          curEdge.private(v, [current:"1", arrow:n.id])
          v.text = c.get("CooperationStep", n.private.score, coc, coc*2) //TODO:bug need to fix third argument for social and endow
          
        }
        
        //HJ
        def score0 = 0
        def score1 = 10
        def score2 = 20
        def score3 = 30
        def score4 = 40
        def score5 = 50
        	
        a.add(v, init, 
              [name: ("Give (-50)"), 
               class: "ab-button a",
               event: [name:"Cooperated", data:[pid:v.id, nid:n.id]],
               result: {
                 curEdge.private(v, 
                   [current:"0", 
                    arrow:n.id + ",green", 
                    cooperation:"1", 
                    animate:curRound + "," + score5*2 + "," + v.getId() + "," + n.getId()])
                 curEdge.coopcount++ 
                 v.private.score -=score5
                 v.private.give=1
                 v.private.coopCount++
                 v.private.totalCoopCount++
                 v.private["coop_" + n.id] = 1
                 //fix bug HJ
                 v.private.neighborReceive = score5*2
                 v.private.payment=score5
                 v.private["totalTo_" + n.getId()]+= score5*2
                 n.private.earned+=score5*2
                 
                 //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
                 v.private["paymentSent_" + n.id] = score5 * 2
                 v.private["amountPaid_" + n.id] = score5
                 
                 v.private.resultMessage+= c.get("ResultsStepIndivid",n.score.substring(0,1),                
               									(v.private.payment),
              									 (v.private.neighborReceive)
                      														 )
                 //end of fix bug
                 
                 //New addition HJ
                 v.private.neighborReceived3+=score5*2
                 if(politicalOrientation){
                   if((n.polOr-1)%3==(v.polOr-1)%3){//same political orientation
                     v.private.neighborReceived3IN+=score5*2
                     v.private.numInGroupNeighbors+=1
                   }else{ //different political orientation
                     v.private.neighborReceived3OUT+=score5*2
                     v.private.numOutGroupNeighbors+=1
                   }
                 }
                 //reset in maketies steps
                 //end of new addition
                 

               }], 
              [name: ("Give (-40)"), 
               class: "ab-button a",
               event: [name:"Cooperated", data:[pid:v.id, nid:n.id]],
               result: {
                 curEdge.private(v, 
                   [current:"0", 
                    arrow:n.id + ",green", 
                    cooperation:"1", 
                    animate:curRound + "," + score4*2 + "," + v.getId() + "," + n.getId()])
                 curEdge.coopcount++ 
                 v.private.score -= score4
                 v.private.give=1
                 v.private.coopCount++
                 v.private.totalCoopCount++
                 v.private["coop_" + n.id] = 1
                //fix bug HJ
                 v.private.neighborReceive = score4*2
                 v.private.payment=score4
                 v.private["totalTo_" + n.getId()]+= score4*2
                 
                 //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
                 v.private["paymentSent_" + n.id] = score4 * 2
                 v.private["amountPaid_" + n.id] = score4
                 
                 n.private.earned+=score4*2
                 v.private.resultMessage+= c.get("ResultsStepIndivid",n.score.substring(0,1),                
               									(v.private.payment),
              									 (v.private.neighborReceive)
                      														 )
                 //end of fix bug
                 
                 //New addition HJ
                 v.private.neighborReceived3+=score4*2
                 if(politicalOrientation){
                   if((n.polOr-1)%3==(v.polOr-1)%3){//same political orientation
                     v.private.neighborReceived3IN+=score4*2
                     v.private.numInGroupNeighbors+=1
                   }else{ //different political orientation
                     v.private.neighborReceived3OUT+=score4*2
                     v.private.numOutGroupNeighbors+=1
                   }
                 }
                 //reset in maketies steps
                 //end of new addition
               }], 
              [name: ("Give (-30)"), 
               class: "ab-button a",
               event: [name:"Cooperated", data:[pid:v.id, nid:n.id]],
               result: {
                 curEdge.private(v, 
                   [current:"0", 
                    arrow:n.id + ",green", 
                    cooperation:"1", 
                    animate:curRound + "," + score3*2 + "," + v.getId() + "," + n.getId()])
                 curEdge.coopcount++ 
                 v.private.score -= score3
                 v.private.give=1
                 v.private.coopCount++
                 v.private.totalCoopCount++
                 v.private["coop_" + n.id] = 1
                //fix bug HJ
                 v.private.neighborReceive = score3*2
                 v.private.payment=score3
                 v.private["totalTo_" + n.getId()]+= score3*2
                 
                 //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
                 v.private["paymentSent_" + n.id] = score3 * 2
                 v.private["amountPaid_" + n.id] = score3
                 
                 n.private.earned+=score3*2
                 v.private.resultMessage+= c.get("ResultsStepIndivid",n.score.substring(0,1),                
               									(v.private.payment),
              									 (v.private.neighborReceive)
                      														 )
                 //end of fix bug
                 
                 //New addition HJ
                 v.private.neighborReceived3+=score3*2
                 if(politicalOrientation){
                   if((n.polOr-1)%3==(v.polOr-1)%3){//same political orientation
                     v.private.neighborReceived3IN+=score3*2
                     v.private.numInGroupNeighbors+=1
                   }else{ //different political orientation
                     v.private.neighborReceived3OUT+=score3*2
                     v.private.numOutGroupNeighbors+=1
                   }
                 }
                 //reset in maketies steps
                 //end of new addition
               }], 
              [name: ("Give (-20)"), 
               class: "ab-button a",
               event: [name:"Cooperated", data:[pid:v.id, nid:n.id]],
               result: {
                 curEdge.private(v, 
                   [current:"0", 
                    arrow:n.id + ",green", 
                    cooperation:"1", 
                    animate:curRound + "," + score2*2 + "," + v.getId() + "," + n.getId()])
                 curEdge.coopcount++ 
                 v.private.score -= score2
                 v.private.give=1
                 v.private.coopCount++
                 v.private.totalCoopCount++
                 v.private["coop_" + n.id] = 1
                 //fix bug HJ
                 v.private.neighborReceive = score2*2
                 v.private.payment=score2
                 v.private["totalTo_" + n.getId()]+= score2*2
                 
                 //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
                 v.private["paymentSent_" + n.id] = score2 * 2
                 v.private["amountPaid_" + n.id] = score2
                 
                 n.private.earned+=score2*2
                 v.private.resultMessage+= c.get("ResultsStepIndivid",n.score.substring(0,1),                
               									(v.private.payment),
              									 (v.private.neighborReceive)
                      														 )
                 //end of fix bug
                 
                 //New addition HJ
                 v.private.neighborReceived3+=score2*2
                 if(politicalOrientation){
                   if((n.polOr-1)%3==(v.polOr-1)%3){//same political orientation
                     v.private.neighborReceived3IN+=score2*2
                     v.private.numInGroupNeighbors+=1
                   }else{ //different political orientation
                     v.private.neighborReceived3OUT+=score2*2
                     v.private.numOutGroupNeighbors+=1
                   }
                 }
                 //reset in maketies steps
                 //end of new addition
               }], 
              [name: ("Give (-10)"), 
               class: "ab-button a",
               event: [name:"Cooperated", data:[pid:v.id, nid:n.id]],
               result: {
                 curEdge.private(v, 
                   [current:"0", 
                    arrow:n.id + ",green", 
                    cooperation:"1", 
                    animate:curRound + "," + score1*2 + "," + v.getId() + "," + n.getId()])
                 curEdge.coopcount++ 
                 v.private.score -= score1
                 v.private.give=1
                 v.private.coopCount++
                 v.private.totalCoopCount++
                 v.private["coop_" + n.id] = 1
                 //fix bug HJ
                 v.private.neighborReceive = score1*2
                 v.private.payment=score1
                 v.private["totalTo_" + n.getId()]+= score1*2
                 
                 //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
                 v.private["paymentSent_" + n.id] = score1 * 2
                 v.private["amountPaid_" + n.id] = score1
                 
                 n.private.earned+=score1*2
                 v.private.resultMessage+= c.get("ResultsStepIndivid",n.score.substring(0,1),                
               									(v.private.payment),
              									 (v.private.neighborReceive)
                      														 )
                 //end of fix bug
                 
                 //New addition HJ
                 v.private.neighborReceived3+=score1*2
                 if(politicalOrientation){
                   if((n.polOr-1)%3==(v.polOr-1)%3){//same political orientation
                     v.private.neighborReceived3IN+=score1*2
                     v.private.numInGroupNeighbors+=1
                   }else{ //different political orientation
                     v.private.neighborReceived3OUT+=score1*2
                     v.private.numOutGroupNeighbors+=1
                   }
                 }
                 //reset in maketies steps
                 //end of new addition
               }], 
              [name: "Keep (0)", 
               class: "ab-button b",
               event: [name:"Defected", data:[pid:v.id, nid:n.id]],
               result: {
                 curEdge.private(v, 
                   [current:"0", 
                    arrow:n.id + ",red",
                    cooperation:"-1"])
                 v.private.defectCount++
                 v.private.totalDefectCount++
                 v.private["coop_" + n.id] = -1
                 v.text += c.get("Wait")
                 //fix bug HJ
                 v.private.neighborReceive = score0*2
                 v.private.payment=score0
                 v.private["totalTo_" + n.getId()]+= score0*2
                 
                 //Shrey Fix for text for amount sent and neighbor received being wrong for individual choice.
                 v.private["paymentSent_" + n.id] = score0 * 2
                 v.private["amountPaid_" + n.id] = score0
                 
                 n.private.earned+=score0*2
                 v.private.resultMessage+= c.get("ResultsStepIndivid",n.score.substring(0,1),                
               									(v.private.payment),
              									 (v.private.neighborReceive)
                      														 )
                 //end of fix bug
                 
                 //New addition HJ
                 v.private.neighborReceived3+=score0*2
                 if(politicalOrientation){
                   if((n.polOr-1)%3==(v.polOr-1)%3){//same political orientation
                     v.private.neighborReceived3IN+=score0*2
                     v.private.numInGroupNeighbors+=1
                   }else{ //different political orientation
                     v.private.neighborReceived3OUT+=score0*2
                     v.private.numOutGroupNeighbors+=1
                   }
                 }
                 //reset in maketies steps
                 //end of new addition
               }]) 
        
          				v.private.give=0 
                        
        				
        
      } // v.neighbors.each
    } // if (individualChoice)
    
    if (!individualChoice) {
      int total=0
      v.neighbors.each{j->
        total+=j.private.score
      }
      	v.text=c.get("Blank")
      //v.neighbors.each { x->
       // v.text+=c.get("CooperationMulti",x.score.substring(0,1),x.private.score)
      //}
      
      def score = 0
      def score1 = 0
      def score2 = 0
      def score3 = 0
      def score4 = 0
      def score5 = 0
      
      // for displaying amount of points neighbors will get when giving different amounts (social inequality)
      if (socialcapital) {
        if (v.private.score < 6000) {
          score = v.private.score / 1000 as double
        } else { // when endowment reaches above 6000, points to give to neighbors start to decrease with the equation, so it is held constant
          score = 6 as double
        }
        score1 = Math.round((10/50.0) * (84.4 + (16.8*score) - (1.36*score*score))) // calculating scores for social inequality
        score2 = Math.round((20/50.0) * (84.4 + (16.8*score) - (1.36*score*score)))
        score3 = Math.round((30/50.0) * (84.4 + (16.8*score) - (1.36*score*score)))
        score4 = Math.round((40/50.0) * (84.4 + (16.8*score) - (1.36*score*score)))
        score5 = Math.round((50/50.0) * (84.4 + (16.8*score) - (1.36*score*score)))
        v.text += c.get("CooperationStepGroup", v.neighbors.count(),score1,score2,score3,score4,score5)
      } else {
        // for displaying amount of points neighbors will get when giving different amounts (social equality)
        score1 = 20
        score2 = 40
        score3 = 60
        score4 = 80
        score5 = 100
        v.text += c.get("CooperationStepGroup", v.neighbors.count(),score1,score2,score3,score4,score5)
      }
      
      a.add(v, {}, 
            [name: "Give 0 to Each", 
         class: "ab-button a",
         event: [name:"Defected", data:[pid:v.id]],
         result: {
           v.neighbors.each { n->
             def curEdge = g.getEdge(v, n)
             curEdge.private(v, 
               [current:"0", 
                arrow:n.id + ",red",
                cooperation:"-1"])
             v.private["coop_" + n.id] = -1
             v.private["paymentSent_" + n.id] = 0
             v.private["amountPaid_" + n.id] = 0
           }
           v.private.payment = 0
           v.private.neighborReceive = 0
           v.private.defectCount += (v.neighbors.count())
           v.private.totalDefectCount += (v.neighbors.count())
           v.text += c.get("Wait")
         }],
        [name: ("Give 10 to Each"), 
         class: "ab-button a",
         event: [name:"Cooperated", data:[pid:v.id]],
         result: {
           v.neighbors.each { n->
             def curEdge = g.getEdge(v, n)
               curEdge.private(v, 
               [current:"0", 
                arrow:n.id + ",green", 
                cooperation:"1", 
                animate:curRound + "," + score1 + "," + v.getId() + "," + n.getId()])
             
             curEdge.coopcount++ 
             v.private["coop_" + n.id] = 1
           }
           v.private.paid = 1
           v.private.score -= (10 * v.neighbors.count())
           v.private.payment = 10
           v.neighbors.each{j->
        		j.private.earned += score1
               	v.private["totalTo_" + j.id] += score1
             	v.private.neighborReceive = score1
             
             	//Shrey's changes
             	v.private["paymentSent_" + j.id] = score1
                v.private["amountPaid_" + j.id] = 10
      		}
           v.private.coopCount += (v.neighbors.count())
           v.private.totalCoopCount += (v.neighbors.count())
           v.text += c.get("Wait")
         }], 
            [name: ("Give 20 to Each"), 
         class: "ab-button a",
         event: [name:"Cooperated", data:[pid:v.id]],
         result: {
           v.neighbors.each { n->
             def curEdge = g.getEdge(v, n)
               curEdge.private(v, 
               [current:"0", 
                arrow:n.id + ",green", 
                cooperation:"1", 
                animate:curRound + "," + score2 + "," + v.getId() + "," + n.getId()])
             curEdge.coopcount++ 
             v.private["coop_" + n.id] = 1
           }
           v.private.paid = 2
           v.private.score -= (20 * v.neighbors.count())
           v.private.payment = 20
           v.neighbors.each{j->
        		j.private.earned += score2
               	v.private["totalTo_" + j.id] += score2
               	v.private.neighborReceive = score2
             
             	//Shrey's changes
             	v.private["paymentSent_" + j.id] = score2
                v.private["amountPaid_" + j.id] = 20
      		}
           v.private.coopCount += (v.neighbors.count())
           v.private.totalCoopCount += (v.neighbors.count())
           v.text += c.get("Wait")
         }], 
            [name: ("Give 30 to Each"), 
         class: "ab-button a",
         event: [name:"Cooperated", data:[pid:v.id]],
         result: {
           v.neighbors.each { n->
             def curEdge = g.getEdge(v, n)
               curEdge.private(v, 
               [current:"0", 
                arrow:n.id + ",green", 
                cooperation:"1", 
                animate:curRound + "," + score3 + "," + v.getId() + "," + n.getId()])
             curEdge.coopcount++ 
             v.private["coop_" + n.id] = 1
           }
           v.private.paid = 3
           v.private.score -= (30 * v.neighbors.count())
           v.private.payment = 30
           v.neighbors.each{j->
        		j.private.earned += score3
              	v.private["totalTo_" + j.id] += score3
               	v.private.neighborReceive = score3
             
             	//Shrey's changes
             	v.private["paymentSent_" + j.id] = score3
                v.private["amountPaid_" + j.id] = 30
      		}
           v.private.coopCount += (v.neighbors.count())
           v.private.totalCoopCount += (v.neighbors.count())
           v.text += c.get("Wait")
         }], 
            [name: ("Give 40 to Each"), 
         class: "ab-button a",
         event: [name:"Cooperated", data:[pid:v.id]],
         result: {
           v.neighbors.each { n->
             def curEdge = g.getEdge(v, n)
               curEdge.private(v, 
               [current:"0", 
                arrow:n.id + ",green", 
                cooperation:"1", 
                animate:curRound + "," + score4 + "," + v.getId() + "," + n.getId()])
             curEdge.coopcount++ 
             v.private["coop_" + n.id] = 1
           }
           v.private.paid = 4
           v.private.score -= (40 * v.neighbors.count())
           v.private.payment = 40
           v.neighbors.each{j->
        		j.private.earned += score4
             	v.private["totalTo_" + j.id] += score4
             	v.private.neighborReceive = score4
             
             	//Shrey's changes
             	v.private["paymentSent_" + j.id] = score4
                v.private["amountPaid_" + j.id] = 40
      		}
           v.private.coopCount += (v.neighbors.count())
           v.private.totalCoopCount += (v.neighbors.count())
           v.text += c.get("Wait")
         }], 
            [name: ("Give 50 to Each"), 
         class: "ab-button a",
         event: [name:"Cooperated", data:[pid:v.id]],
         result: {
           v.neighbors.each { n->
             def curEdge = g.getEdge(v, n)
               curEdge.private(v, 
               [current:"0", 
                arrow:n.id + ",green", 
                cooperation:"1", 
                animate:curRound + "," + score5 + "," + v.getId() + "," + n.getId()])
             curEdge.coopcount++ 
             v.private["coop_" + n.id] = 1
           }
           v.private.paid = 5
           v.private.score -= (50 * v.neighbors.count())
           v.private.payment = 50
           v.neighbors.each{j->
        		j.private.earned += score5
              	v.private["totalTo_" + j.id] += score5
              	v.private.neighborReceive = score5
             
             	//Shrey's changes
             	v.private["paymentSent_" + j.id] = score5
                v.private["amountPaid_" + j.id] = 50
      		}
           v.private.coopCount += (v.neighbors.count())
           v.private.totalCoopCount += (v.neighbors.count())
           v.text += c.get("Wait")
         }]
        ) 
    } // if (!individualChoice)
  } // g.V.filter{it.active}.each 
} // cooperationStep.run



cooperationStep.done = {
  println "cooperationStep.done"
   log("Current Round: " + curRound, experimentData)
  g.V.filter{it.active}.each{ v->
    log("Player Id: " + v.score.substring(0,1), experimentData)
    log("Player Score at the start of the round: " + (v.private.score + (v.private.payment * v.neighbors.count())), experimentData) //This will only work for group choices
    println "private score" + v.private.score
    println "private payment" + v.private.payment
    println "private earned" + v.private.earned
    log("Player Score at the end of the round: " + (v.private.score + v.private.earned), experimentData)
    log("Neighbor Count: " + v.neighbors.count(), experimentData)
    def neighborList = ""
    def neighborScoreList = ""
    v.neighbors.each { n->
      neighborList += n.score + ", "
      neighborScoreList += n.private.neighborReceive + ", "
    }
    log("Neighbor List: " + neighborList, experimentData)
    log("The points received from neighbors: " + neighborScoreList, experimentData)
    log("The points Neighbors received this round from this player: " + v.private.neighborReceive, experimentData)
    
  }
  //Pause here to allow animation to complete
  (new Timer()).runAfter(1500) { resultsStep.start() }
}