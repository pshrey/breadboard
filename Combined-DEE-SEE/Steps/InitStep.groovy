// Global variables
curRound = 0
startAt = null
beginGame = true
initStep = stepFactory.createStep()

initStep.run = {
  println "initStep.run"
  a.addEvent("StepStart", ["step":"initStep","curRound":curRound])
  curStep = "init"
  
  g.V.each { v->
    v.timers = [:]
  }

  curRound = 1
  // Add AI players
  if (addAI) { g.addAI(a,20) }
  
  // Start dropping players
   a.setDropPlayers(true)
  
  // Drop players who haven't completed the tutorial
 
  g.V.filter{!it.active && it.ai != 1}.each { v->
    a.remove(v)
    v.text = c.get("FailedTest", g.getSubmitForm(v, "0", "failed_test", amtSandbox, true))
  }
  
  //changed logic for testing, should just ber it.active 
  g.V.filter{it.active || it.ai == 1}.each { v->
    def init = {
      v.text = c.get("PleaseWait") + "<p><strong>Click Begin to start</strong></p>"
    }
    a.add(v, init, [name: "Begin", 
                    event: [name:"ClickedBegin", data:[pid:v.id]],
                    result: {
                      v.text = c.get("PleaseWait") + "<p><strong>Thank you, the game will begin in a moment.</strong></p>"
                    }])
  }
}

initStep.done = {
  println "initStep.done"
  graphStep.start()
}
