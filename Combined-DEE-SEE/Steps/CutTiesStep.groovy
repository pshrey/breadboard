cutTiesStep = stepFactory.createStep()

cutTiesStep.run = {
  println "cutTiesStep.run: ${curRound}"
  a.addEvent("StepStart", ["step":"cutTiesStep","curRound":curRound])
  curStep = "cutTies"
  // Dropped players cut all their ties in this round
  g.V.filter{it.dropped}.each { droppedPlayer->
    log("CutTies Round",experimentData)
    droppedPlayer.private.cutCount = 0
    droppedPlayer.private.nCutCount = 0
    droppedPlayer.active = false
    log("Player " + droppedPlayer.score + " got dropped this round", experimentData)
    droppedPlayer.neighbors.each { n->
      def curEdge = g.getEdge(droppedPlayer, n)
      curEdge.tobreak = true
      curEdge.breakTarget << n
      curEdge.private(droppedPlayer, [breaking:1, arrow:n.id])
      droppedPlayer.private.cutCount++
      n.private.nCutCount++
      log("Player " + droppedPlayer.score + " cut tie with player " + n.score, experimentData)
    }
  }
  
  //new 
  if(dynamic){
    if (curRound%3 == 0) {
    log("CutTies Round",experimentData)
    g.V.filter{it.active}.each { v->
      v.private.cutCount = 0
      v.private.nCutCount = 0
      if (v.neighbors.count() > 0) {      
        def choices = []
        v.text = c.get("CutTiesStep")
        v.neighbors.each { n->
          //following line only works for groupchoice
          v.text += c.get("CutTiesNeighScores", n.score.substring(0,1),n.private.payment,(n.private.neighborReceive),n.private["totalTo_" + v.id])
          def label = n.score.substring(0,1)
          def choice = [name: label,
                        //class: "cut-button " + ((n["coop_" + v.id] == 1) ? "a" : "b"),
                        class: "cut-button ",
                        event: [name:"CutTie", data:[pid:v.id, nid:n.id]],
                        result: {
                          def curEdge = g.getEdge(v, n)
                          curEdge.tobreak = true
                          curEdge.breakTarget << n
                          curEdge.private(v, [breaking:1, arrow:n.id])
                          v.private.cutCount++
                          n.private.nCutCount++
                          v.text += c.get("Wait")
                          log("Player " + v.score + " chose to cut tie with neighbor " + n.score, experimentData)
                        }]
              choices << choice
            } //v.neighbors.each
            def noPlayer = [name: "No player", event:[name:"CutNoTie",data:[pid:v.id]], result: {}]
            choices << noPlayer 
            a.add(v, *choices)
        } // if (v.neighbors.count() > 0)
      } // g.V.each
    }//new curRound
  }// dynamic
} // cutTiesStep.run

cutTiesStep.done = {
  println "cutTiesStep.done"
  cutTiesResultStep.start()
}
