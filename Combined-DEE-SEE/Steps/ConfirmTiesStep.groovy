confirmTiesStep = stepFactory.createStep()

confirmTiesStep.run = {
  println "confirmTiesStep.run: ${curRound}"
  a.addEvent("StepStart", ["step":"confirmTiesStep","curRound":curRound])
  curStep = "confirmTies"

  //new 
  if (curRound % 3 == 0) {
    g.V.filter{it.active == true}.each { v->
      v.toConfirm.each { n->
        log("Player " + n.score + " requested to have a tie with player " + v.score, experimentData)
      }
      v.toConfirm.each { n->
        if (n.toConfirm.contains(v)) {
        	log("Player " + n.score + " mutually accepted a tie with player " + v.score, experimentData)
        }
      }
    }
    g.V.filter{it.active == true}.each { v->
      v.toConfirm.each { n->
        g.addEdge(v, n, "connected")
        def curEdge = g.getEdge(v, n)
        setEdgeDefaults(curEdge)
        // Arrow points in direction of intention
        curEdge.private(v, [arrow:v.id])
        curEdge.private(n, [arrow:v.id])

        if (n.toConfirm.contains(v)) {
          //No need to confirm, both players chose the other
          n.toConfirm.remove(v)
          curEdge.private(v, [making:"0",current:"0"])
          curEdge.private(n, [making:"0",current:"0"])
          curEdge.toconfirm = false
        } else {
          curEdge.toconfirm = true

          def init = {
            v.text = c.get("ConfirmTiesStep", n.private.payment, n.private.neighborReceive)
            def coopRate = getCoopRate(v, n)         
            def svg = """
    <svg width="110" height="110">
      <g class="node client" transform="translate(55,55)">
        <circle class="node client ${coopRate.color}" r="50"></circle>
        <text class="node client" style="text-anchor: middle; font-size: 14pt;">${coopRate.score}</text>
      </g>
    </svg>
            """
            v.text += svg
            curEdge.private(v, [making:"1",current:"1", arrow:n.id])
          }

          a.add(v, init,
                [name:"Yes",
                 event: [name:"ConfirmTie", data:[pid:v.id, nid:n.id]],
                 result: { 
                   curEdge.private(v, [making:"0",current:"0",arrow:"both"])
                   curEdge.tomake = true
                   curEdge.toconfirm = false
                   v.acceptOfferTies++
                   n.offerAcceptTies++
                   v.text += c.get("Wait")
                   log("Player " + v.score + " accepts a tie with player " + n.score, experimentData)
                 }],
                [name: "No",
                 event: [name:"RejectTie", data:[pid:v.id, nid:n.id]],
                 result: {
                   curEdge.private(v, [making:"0",current:"0",breaking:"1",arrow:v.id])
                   curEdge.toconfirm = false
                   curEdge.tobreak = true
                   v.rejectOfferTies++
                   n.offerRejectTies++
                   v.text += c.get("Wait")
                   log("Player " + v.score + " rejects a tie with player " + n.score, experimentData)
                 }])
        }

      }
    }
  }
 } //new curRound
confirmTiesStep.done = {
  println "confirmTiesStep.done"
  makeTiesResultStep.start()
}
