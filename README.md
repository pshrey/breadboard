# Introduction
This repository is for keeping track of breadboard code for the project and for resources related to breadboard.

# Resources
Breadboard Wiki: [https://github.com/human-nature-lab/breadboard/wiki](https://github.com/human-nature-lab/breadboard/wiki)

# Note for MacOS Users
If you have trouble running  the ./breadboard.sh file where the localhost:9000 doesnt connect.
Then, check what version of jdk you have installed using `/usr/libexec/java_home -V.`
If you do not have it installed, then install it using [http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
Then change the version of your terminal to it by using ```export JAVA_HOME=`/usr/libexec/java_home -v 1.8` ```

# Changelog Format

```
# Changelog
## [Version Number] - Date
### Added
 - List things added in this commit
### Changed
 - List things changed in this commit
### Removed
 - List things removed in this commit
```
